FROM tensorflow/tensorflow:1.14.0-py3
WORKDIR /root

RUN pip install --upgrade pip
RUN apt-get update

RUN apt-get install -y libsm6 libxext6 libxrender-dev curl
RUN pip install opencv-python pillow matplotlib keras imageai

COPY h5gt.py ./
RUN python3 h5gt.py coco yolo

# COPY *.h5 ./

COPY inp/*.* ./inp/
RUN mkdir ./outp

COPY *.py ./

CMD ["python3", "anim.py"]
# CMD ["bash"]
