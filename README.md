My help: https://itproger.com/news/174

Get coco.h5: https://github.com/OlafenwaMoses/ImageAI/releases/download/1.0/resnet50_coco_best_v2.0.1.h5
Get yolo.h5: https://github.com/OlafenwaMoses/ImageAI/releases/download/1.0/yolo.h5
All *.h5: https://github.com/OlafenwaMoses/ImageAI/releases/tag/1.0/

Get yolo.h5 (hard method):
	0) my help: https://github.com/JudasDie/deeplearning.ai/issues/2#issuecomment-360502406 (not good, so read onward)
	1) make CMD ["bash"] in this Dockerfile and call ./run.sh
	2) wait until pull in docker
	3) "apt install curl git"
	4) clone https://github.com/allanzelener/YAD2K
	5) download https://github.com/pjreddie/darknet/blob/master/cfg/yolov2.cfg as "yolo.cfg" to YAD2K folder
	6) download https://pjreddie.com/media/files/yolo.weights as "yolo.weights" to YAD2K folder
	7) in YAD2K folder run "python3 yad2k.py yolo.cfg yolo.weights model_data/yolo.h5"
	7.1) if error, check other yolo*.cfg at https://github.com/pjreddie/darknet/blob/master/cfg
	8) before exit from docker shell (!) run "docker cp acid-proj:/root/YAD2K/model_data/yolo.h5 ." on host machine
	9) exit from docker
