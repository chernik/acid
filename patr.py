import json, math, time

def getDiff(fa, fb, eps):
	diff = [None] * len(fb)
	for ia in range(len(fa)):
		min = None
		for ib in range(len(fb)):
			if fa[ia]['class'] != fb[ib]['class']:
				continue
			dv = [fb[ib]['box'][ie] - fa[ia]['box'][ie] for ie in range(4)]
			dv = [(dv[ie] + dv[ie+2]) / 2 for ie in range(2)]
			dv = math.sqrt(sum([e*e for e in dv]))
			if dv > eps:
				continue
			if diff[ib] != None:
				if diff[ib][1] < dv:
					continue
			if min != None:
				if min[2] < dv:
					continue
			min = [ib, ia, dv]
		if min != None:
			diff[min[0]] = min[1:]
	return diff

def getTraces(objs, eps, nf):
	traces = []
	i = len(objs)
	while True:
		i -= 1
		if i == 0:
			break
		used = []
		for n in range(1, nf+1):
			if i-n < 0:
				continue
			diff = getDiff(objs[i-n], objs[i], eps*n)
			for ib in range(len(diff)):
				if diff[ib] == None:
					continue
				if ib in used:
					continue
				used.append(ib)
				ftr = None
				for tr in traces:
					if tr[0]-len(tr)+2 == i and tr[-1] == ib:
						ftr = tr
						break
				if ftr == None:
					ftr = [i, ib]
					traces.append(ftr)
				ftr += [None] * (n-1) + [diff[ib][0]]
	traces = [[tr[0]-len(tr)+2] + list(reversed(tr[1:]))
		for tr in traces if len(tr) > 3]
	traces.reverse()
	return traces

def fillTraces(traces, objs):
	for tr in traces:
		while True:
			try:
				ia = tr.index(None)
			except:
				break
			ib = ia+1
			ia = ia-1
			while tr[ib] == None:
				ib += 1
			oa, ob = [objs[e+tr[0]-1][tr[e]] for e in [ia, ib]]
			os = Anim.animate('linear', 'd', oa['box'], ib-ia-1, ob['box'])
			print(os)
			for i in range(1, ib-ia):
				fm = objs[ia+i+tr[0]-1]
				tr[ia+i] = len(fm)
				fm.append({
					'class': oa['class'],
					'percent': 0.0,
					'box': os[i]
				})
	pass

class Anim:
	class __Hidden:
		pass
	def animate(aniType, valType, *frs):
		"""
		animate(aniType, valType, frame1, cntFrames12, frame2, ...)
		"""
		# check aniType
		animator = Anim.__Animators.__dict__.get(
			aniType
		)
		if animator == None:
			raise Exception('Wrong aniType')
		# check frs len
		if len(frs) < 3:
			raise Exception('Wrong count of frames')
		if (len(frs) % 2) == 0:
			raise Exception('Wrong count of frames')
		return Anim.__animate(animator, valType, *frs)
	def __animate(animator, valType, *frs):
		if type(frs[0]) in [type([]), type(())]:
			return Anim.__animateList(animator, valType, *frs)
		if type(frs[0]) in [type({})]:
			return Anim.__animateDict(animator, valType, *frs)
		return Anim.__animateLow(animator, valType, *frs)
	def __animateList(animator, valType, *frs):
		if type(valType) in [type([]), type(())]:
			if len(frs[0]) != len(valType):
				raise Exception('Wrong len of valType')
		else:
			valType = [valType] * len(frs[0])
		res = []
		for vali in range(len(frs[0])):
			frsp = []
			for fri in range(len(frs)):
				if (fri % 2) == 0:
					if type(frs[fri]) not in [type([]), type(())]:
						raise Exception('Frame %d is not list or tuple' % (fri / 2))
					if len(frs[fri]) != len(frs[0]):
						raise Exception('Wrong len of val, look %d frame' % (fri / 2))
					frsp.append(frs[fri][vali])
				else:
					frsp.append(frs[fri])
			res.append(Anim.__animate(animator, valType[vali], *frsp))
		return [[el[fri] for el in res] for fri in range(len(res[0]))]
	def __animateDict(animator, valType, *frs):
		if type(valType) in [type({})]:
			ks = list(valType)
			ks.sort()
			valType = [['i', valType[k]] for k in valType]
		else:
			ks = list(frs[0])
			ks.sort()
			valType = [['i', valType]] * len(frs[0])
		frs = list(frs)
		for i in range(0, len(frs), 2):
			if type(frs[i]) not in [type({})]:
				raise Exception('Frame %d is not dict' % (i / 2))
			ksi = list(frs[i])
			ksi.sort()
			if ks != ksi:
				raise Exception('Frame %d has wrong keys' % (i / 2))
			frs[i] = [[k, frs[i][k]] for k in ksi]
		frs = Anim.__animateList(animator, valType, *frs)
		for i in range(len(frs)):
			fr = {}
			[fr.__setitem__(e[0], e[1]) for e in frs[i]]
			frs[i] = fr
		return frs
	def __animateLow(animator, valType, *frs):
		if valType == None:
			valType = ''
		if 'i' in valType:
			return Anim.__animateLow(Anim.__Animators.const, '', *frs)
		hasCast = ''
		if 'd' in valType:
			hasCast = 'd'
			valType = valType.replace('d', '')
		if 'r' in valType:
			# TODO: rotate
			valType = valType.replace('r', '')
		hidden = Anim.__Hidden()
		frscnt = [frs[i] for i in range(1, len(frs), 2)]
		frs = [frs[i] for i in range(0, len(frs), 2)]
		res = [frs[0]]
		for i in range(len(frs)-1):
			res += animator(
				valType, hidden,
				frs, frscnt, i
			) + [frs[i+1]]
		if hasCast == 'd':
			res = [round(e) for e in res]
		return res
	class __Animators:
		def const(valType, hidden, frs, frscnt, fri):
			return [frs[fri]] * frscnt[fri]
		def linear(valType, hidden, frs, frscnt, fri):
			return [
				frs[fri] + (frs[fri+1]-frs[fri])*i/(frscnt[fri]+1)
				for i in range(1, frscnt[fri]+1)
			]
	def test():
		"""
		Look this code for understanding
		"""
		assert [3, 4, 5] == Anim.animate('linear', 'd', 3, 1, 5)
		assert list(range(3, 10)) == Anim.animate('linear', 'd', 3, 2, 6, 2, 9)
		assert [[i, i+2] for i in range(1, 8)] == Anim.animate('linear', 'd', [1,3], 5, [7,9])
		assert [{'a':i,'b':i+2} for i in range(1, 8)] == Anim.animate('linear', 'd', {'a':1,'b':3}, 5, {'b':9,'a':7})
		assert [[2,4], 6.5, [3,5]] == Anim.animate('linear', ['d', '', 'd'], [(1,3), 5, (2,4)], 1, [(3,5), 8, (4,6)])[1]

def loadTraces(fobjs):
	with open(fobjs, 'r', encoding='utf8') as f:
		objs = json.load(f)
	trs = getTraces(objs, 5, 5)
	fillTraces(trs, objs)
	return (trs, objs)

def test():
	with open('inp/video2.json', 'r', encoding='utf8') as f:
		objs = json.load(f)
	t = time.time()
	traces = getTraces(objs, 10, 3)
	t = time.time() - t
	print('Time for create traces %f sec' % t)
	t = time.time()
	fillTraces(traces, objs)
	t = time.time() - t
	print('Time for fill %f sec' % t)
	with open('outp/video.json', 'w', encoding='utf8') as f:
		json.dump(traces, f)

Anim.test()
# test()
