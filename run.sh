#/bin/bash

docker build -t acid-proj .
docker run -it --name acid-proj acid-proj

docker cp acid-proj:/root/outp/photo_.jpg ./outp/
docker cp acid-proj:/root/outp/video_.avi ./outp/
docker cp acid-proj:/root/outp/video_.mp4 ./outp/
docker cp acid-proj:/root/outp/video.json ./outp/

docker rm acid-proj
