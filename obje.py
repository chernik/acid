from imageai.Detection import ObjectDetection, VideoObjectDetection
import os, time, json, cv2

exec_path = os.getcwd()

def countFrames(fin):
	v = cv2.VideoCapture(fin)
	c = int(v.get(cv2.CAP_PROP_FRAME_COUNT))
	v.release()
	return c

class DetCoco:
	def __init__(self, datafile = 'coco.h5'):
		print('Load COCO detector')
		print('-' * 20)
		t = time.time()
		self.__detector = ObjectDetection()
		self.__detector.setModelTypeAsRetinaNet()
		self.__detector.setModelPath(os.path.join(
			exec_path, datafile)
		)
		self.__detector.loadModel()
		t = time.time() - t
		print('-' * 20)
		print('Loaded in %f seconds' % t)
		self.__ts = []
	def detectPhoto(self, fin, fout, acc = 20):
		t = time.time()
		lst = self.__detector.detectObjectsFromImage(
			input_image=os.path.join(exec_path, fin),
			output_image_path=os.path.join(exec_path, fout),
			minimum_percentage_probability=acc,
			display_percentage_probability=True,
			display_object_name=False
		)
		t = time.time() - t
		self.__ts.append(t)
		return lst
	def outTimes(self):
		print(self.__ts)
		m = sum(self.__ts) / len(self.__ts)
		print('Avg time of COCO detector is %f seconds' % m)

class DetYolo:
	def __init__(self, datafile = 'yolo.h5'):
		print('Load YOLO detector')
		print('-' * 20)
		t = time.time()
		detector = VideoObjectDetection()
		detector.setModelTypeAsYOLOv3()
		detector.setModelPath(os.path.join(exec_path, datafile))
		detector.loadModel()
		t = time.time() - t
		print('-' * 20)
		print('Loaded in %f seconds' % t)
		self.__ts = []
		self.__detector = detector
	def detectVideo(self, fin, fout, acc = 20):
		frames = countFrames(fin)
		t = [time.time()]
		lst = []
		def perFrame(frame, res, cnt_dict):
			lst.append(res)
			# print('For frame %d cnt_dict is %s' % (frame, str(cnt_dict)))
			tt = time.time()
			s = (tt - t[0]) * (frames - frame)
			t[0] = tt
			print('Frame %d/%d, est %.2f secs | %.2f mins | %.2f hours' % (frame, frames, s, s / 60, s / 3600), end = '\r')
		t2 = time.time()
		fout = self.__detector.detectObjectsFromVideo(
			input_file_path=os.path.join(exec_path, fin),
			output_file_path=os.path.join(exec_path, fout),
			minimum_percentage_probability=acc,
			# frames_per_second=10,
			# log_progress=True,
			per_frame_function=perFrame
		)
		t2 = time.time() - t2
		self.__ts.append(t2)
		return {
			'result': lst,
			'fout': fout
		}
	def outTimes(self):
		print(self.__ts)
		m = sum(self.__ts) / len(self.__ts)
		print('Avg time of YOLO detector is %f seconds' % m)

def testCoco():
	det = DetCoco()
	print(det.detectPhoto("inp/photo.jpg", "outp/photo_.jpg"))
	det.outTimes()

def testYolo():
	det = DetYolo()
	res = det.detectVideo("inp/video4.mp4", "outp/video_") # output is always 'avi'
	res = [[{
		'class': str(fr['name']),
		'percent': float(fr['percentage_probability']),
		'box': [int(e) for e in fr['box_points']]
	} for fr in mfr] for mfr in res['result']]
	with open("outp/video.json", "w", encoding="utf8") as f:
		json.dump(res, f)
	det.outTimes()

testYolo()
