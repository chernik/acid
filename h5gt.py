import os, sys, time

def downl(ft):
	f = {
		'coco': 'resnet50_coco_best_v2.0.1',
		'yolo': 'yolo'
	}[ft]
	f = 'https://github.com/OlafenwaMoses/ImageAI/releases/download/1.0/%s.h5' % f
	f = 'curl "%s" > %s.htm' % (f, ft)
	os.system(f)
	with open('%s.htm' % ft, 'r', encoding='utf8') as fs:
		f = fs.read()
	os.system('rm %s.htm' % ft)
	f = f.split('href="')[1].split('">redir')[0]
	f = f.replace('&amp;', '&')
	f = 'curl "%s" > %s.h5' % (f, ft)
	t = time.time()
	os.system(f)
	t = time.time() - t
	print('Download [%s] in %.2f secs / %.2f mins / %.2f hours' % (
		ft, t, t / 60, t / 3600
	))

def main():
	[downl(ft) for ft in sys.argv[1:]]

main()
