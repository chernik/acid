import cv2, numpy
import patr

def getVideoMeta(v):
	fps = int(v.get(cv2.CAP_PROP_FPS))
	w = int(v.get(cv2.CAP_PROP_FRAME_WIDTH))
	h = int(v.get(cv2.CAP_PROP_FRAME_HEIGHT))
	frs = int(v.get(cv2.CAP_PROP_FRAME_COUNT))
	return (fps, w, h, frs)

def affineFrame(f, size, order, *args):
	vargs = []
	for i in range(len(order)):
		vargs.append([order[i], args[i]])
	vargs.reverse()
	m = numpy.float32([
		[1,0,0],
		[0,1,0],
		[0,0,1]
	])
	def getMmove(move):
		return numpy.float32([
			[1, 0, move[0]],
			[0, 1, move[1]],
			[0, 0, 1]
		])
	for arg in vargs:
		if arg[0] == 'm':
			m = m.dot(getMmove(arg[1]))
		elif arg[0] == 's':
			m = m.dot(getMmove([e/2 for e in size]))
			m = m.dot(numpy.float32([
				[arg[1][0], 0, 0],
				[0, arg[1][1], 0],
				[0, 0, 1]
			]))
			m = m.dot(getMmove([-e/2 for e in size]))
		elif arg[0] == 'r':
			mr = cv2.getRotationMatrix2D(
				(size[0] / 2, size[1] / 2),
				arg[1], 1.0
			)
			mr = numpy.append(mr, [[0,0,1]], axis = 0)
			m = m.dot(mr)
		else:
			raise Exception('Wrong label [%s]' % arg[0])
	center = (size[0] / 2, size[1] / 2)
	m = numpy.delete(m, 2, axis = 0)
	res = cv2.warpAffine(f, m, size, flags = cv2.INTER_LINEAR)
	return res

def getRecPair(fin, fout):
	vin = cv2.VideoCapture(fin)
	fps, w, h, frs = getVideoMeta(vin)
	print('fps[%d] size[%d X %d]' % (fps, w, h))
	foutT = cv2.VideoWriter_fourcc(*'mp4v')
	vout = cv2.VideoWriter(fout, foutT, fps, (w, h))
	return (vin, vout)

def animVideo(fin, fout):
	vin, vout = getRecPair(fin, fout)
	fps, w, h, frs = getVideoMeta(vin)
	fr = 0
	while True:
		fr += 1
		if fr > 100:
			break
		res, frame = vin.read()
		if not res:
			break
		spd = fps * 2
		rot = (fr % spd) * 360 / spd
		res = affineFrame(frame, (w, h), 'msmr',
			(-w/2, -h/2),
			(0.5, 0.5),
			(w/2, h/2),
			rot
		)
		vout.write(res)
		print('Frame %d/%d' % (fr, frs), end = '\r')
	vin.release()
	vout.release()

class Draw:
	def __color(color):
		res = [int(color[i:i+2], 16) for i in range(1, 2*3+1, 2)]
		res.reverse()
		return tuple(res)

	def line(fr, coors, color, w = 1):
		cv2.line(
			img = fr,
			pt1 = tuple(coors[0:2]),
			pt2 = tuple(coors[2:4]),
			color = Draw.__color(color),
			thickness = w,
			lineType = 8,
			shift = 0
		)

	def box(fr, coors, color, w = 1):
		Draw.line(fr, coors[0:2] + [coors[2], coors[1]], color, w)
		Draw.line(fr, [coors[2], coors[1]] + coors[2:4], color, w)
		Draw.line(fr, coors[2:4] + [coors[0], coors[3]], color, w)
		Draw.line(fr, [coors[0], coors[3]] + coors[0:2], color, w)

def addTraces(fin, fout):
	trs, objs = patr.loadTraces(fin.split('.')[0] + '.json')
	vin, vout = getRecPair(fin, fout)
	frs = getVideoMeta(vin)[3]
	fr = -1
	while True:
		fr += 1
		res, frame = vin.read()
		if not res:
			break
		#if fr > 100:
		#	break
		for tr in trs:
			if tr[0] > fr:
				continue
			if tr[0]+len(tr)-2 < fr:
				continue
			for i in range(1, len(tr)):
				fi = i+tr[0]-1
				if fi > fr:
					break
				bb = objs[fi][tr[i]]['box']
				if fi == fr:
					Draw.box(frame, bb, '#00ff00', 1)
				if i > 1:
					ba = objs[fi-1][tr[i-1]]['box']
					ca, cb = [[
						round((b[bi] + b[bi+2]) / 2) for bi in range(0, 2)
					] for b in [ba, bb]]
					Draw.line(frame, ca + cb, '#ff0000', 3)
		vout.write(frame)
		print('Frame %d/%d' % (fr, frs), end = '\r')
	vin.release()
	vout.release()

def followTraces(fin, fout):
	trs, objs = patr.loadTraces(fin.split('.')[0] + '.json')
	vin, vout = getRecPair(fin, fout)
	fps, w, h, frs = getVideoMeta(vin)
	trsl = len(trs)
	tri = -1
	center = (w/2, h/2)
	vfrs = []
	while True:
		res, frame = vin.read()
		if not res:
			break
		vfrs.append(frame)
	print('Video readed')
	while True:
		tri += 1
		if len(trs) == 0:
			break
		fr = -1
		trmax = []
		for tr in trs:
			if len(tr) > len(trmax):
				trmax = tr
		tr = trmax
		trs.remove(tr)
		while True:
			fr += 1
			if fr < tr[0]:
				continue
			if fr > tr[0]+len(tr)-2:
				break
			#if fr-tr[0] > 150:
			#	break
			frame = vfrs[fr]
			b = objs[fr][tr[fr-tr[0]+1]]['box']
			bc = [(b[bi] + b[bi+2]) / 2 for bi in range(2)]
			bsize = max([b[bi+2]-b[bi] for bi in range(2)])
			vsize = min([w, h])
			bsc = vsize / bsize
			move = [center[bi] - bc[bi] for bi in range(0, 2)]
			res = affineFrame(frame, (w, h), 'ms', move, (bsc, bsc))
			vout.write(res)
			print('Frame %5d/%5d, trace %5d/%5d' % (fr+1, frs, tri+1, trsl), end = '\r')
	vin.release()
	vout.release()

def test():
	#animVideo('inp/video3.mp4', 'outp/video_.mp4')
	#addTraces('inp/video2.mp4', 'outp/video_.mp4')
	followTraces('inp/video3.mp4', 'outp/video_.mp4')
	print('\nSucs')

test()
